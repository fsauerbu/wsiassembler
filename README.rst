Workspace Input Assembler
=================================


The Workspace Input Assembler provides a tool to combine histograms from multiple
ROOT files into a single root file. The single root file represents a layered
view of input files. Histograms located on a higher level hide histograms on a
lower level. Additional the tool allows to transform (e.g. normalize)
histograms.

The Workspace Input Assembler makes the following assumptions and assertions:
 - The histograms in the files follow the naming convention:
   :code:`chcareg/sample/variation`.
 - Files added to a single layer must not have duplicated histogram names


Histogram lookup scheme is illustrated in the following sketch.

 .. image:: lookup.svg


Example
-------

.. code-block:: yaml

    # Internally each histogram lookup has four paramters:
    #   slug, chcareg, sample, variation 
    # By default, slug is None, meaning that the layer hierarchy is respected
    
    layers:
     # Higher layers have precedance and hide histograms in lower levels
    
     - path: ps.root  # List or string with the filename, names are globbed
       slug: ps  # Nickname of the layer, to be used as a reference in other layers
    
       exclude:
         variation_in: [nominal]
         # Lookup is passed to the next layer if any exclude criteria matches
         # Possible arguments are: chcareg_in, sample_in, variation_in
    
     - path: ggHme.root
       exclude:
         variation_in: [nominal]
       only:
         # Lookup is passed to the next layer if any exclude criteria fails
         variation_in: [ggH]
         # Possible arguments are: chcareg_in, sample_in, variation_in, channel_in,
         # category_in, region_in
    
       transform:
         # If a lookup is delegated to this layer, transform the histograms before
         # returning
         - operation: divide_by
             # Divide by another histogram, set the four parameters and start a new
             # lookup at the top of the layer hierarchy. The four parameters are
             # initialized with current values
           variation: "{variation}"  # could be empty, this is the default
         - operation: multiply_by
           variation: nominal
    
     - path: ttHme.root
       only:
         variation_in: [pss]

       reroute:  # Routing is seen from the perspective of data access
                 # An incoming data request (lookup) will be redirect to a
                 # different histogram in the root file.
         - from:
             sample_in: ['ttH_new']
           to:
             sample: ttH

         sample: "other"
    
       transform:
         - operation: divide_by
           variation: "nominal"
         - operation: multiply_by
           slug: base
           variation: "nominal"
    
     - path: fallback.root
       slug: base
    
    output:
     # Option to create filtered output files
     - filepath: lephad.root
       slug: lh
       only:
         channel_in: lephad
     - filepath: hadhad.root
       slug: hh
       only:
         channel_in: hadhad
     - filepath: leplep.root
       slug: ll
       only:
         channel_in: leplep

Quickstart
==========

Install the package using pip

.. code-block:: console

   $ pip install git+https://gitlab.cern.ch/fsauerbu/wsiassembler.git

.. Once deployed to pypi
   $ pip install wsiassembler

Links
=====

 * `GitLab Repository <https://gitlab.cern.ch/fsauerbu/wsiassembler>`_
..  * `Documentation <https://wsiassembler.readthedocs.io/>`_
..  * `pyveu on PyPi <https://pypi.org/project/wsiassembler>`_
