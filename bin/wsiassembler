#!/usr/bin/env python3

import os
import yaml
from pathlib import Path
import argparse
from wsiassembler.pythonprogress import DoingDone
import wsiassembler
from wsiassembler.errors import NoSuchHistogramError

def main(args):
    """Resolve command-line arguments"""
    current_dir = os.getcwd()
    instruction_file = args.instructions
    if instruction_file is None:
        instruction_file = os.path.join(current_dir, "assembly.yml")

    with open(instruction_file) as instr_file_obj:
        instructions = yaml.safe_load(instr_file_obj)

    layers, outputs = wsiassembler.parse_instructions(instructions)

    if args.trace:
        trace(instruction_file, layers, args.query, args.show)
    else:
        store(instruction_file, layers, outputs, args.query,
              verbose=args.verbose)


def store(instruction_file, layers, outputs, queries, verbose=False):
    """Write histograms to output file"""
    print(f"Instructions: {os.path.abspath(instruction_file)}")

    with DoingDone("Reading file indices"):
        layers.dir()

    for output in outputs:
        if output.slug not in queries:
            continue
        queries.remove(output.slug)
        print(f"Output:  {output.output_file}")
        directory = os.path.dirname(output.output_file)
        Path(directory).mkdir(parents=True, exist_ok=True)
        output.store(verbose)

    for query in queries:
        print(f"Output group {query} not found")

def trace(instruction_file, layers, query, show=False):
    """Print trace information for query"""
    if len(query) != 1:
        print("Trace support only a single argument")
        return
    query = query[0]

    print(f"Instructions: {os.path.abspath(instruction_file)}")
    print(f"Trace query:  {query}")

    with DoingDone("Reading file indices"):
        layers.dir()

    print()

    chcareg, sample, variation = wsiassembler.lookup_params(query)
    try:
        layer, filename, hist = layers.lookup(chcareg, sample, variation)
        print(f"Histogram is supplied from layer '{layer}'")
        print(f"Source file: {filename}")

        if show:
            hist.show()
            input("continue?")

    except NoSuchHistogramError:
        print("Histogram not found in any input files")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true",
                        help="Print verbose output")
    parser.add_argument("-i", "--instructions",
                        help="Assembly instrucitons file, defaults to "
                             "assembly.yml in the current directory.")
    parser.add_argument("-t", "--trace", action="store_true",
                        help="Perform a lookup without writing any output "
                             "files. The output arg is treated as lookup "
                             "path.")
    parser.add_argument("-s", "--show", action="store_true",
                        help="Show traced histogram")
    parser.add_argument("query", metavar="QUERY", nargs="+",
                        help="Histogram or output query")

        
    args = parser.parse_args()
    main(args)
