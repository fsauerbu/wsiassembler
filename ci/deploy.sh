#!/bin/bash

# Expects the following environment variables:
# GPG_KEY (as file), TWINE_PASSWORD, TWINE_USERNAME

cat "$GPG_KEY" | gpg --batch --import

pip install twine
twine upload --sign dist/*