#!/bin/bash

set -e

cd example
../bin/wsiassembler all
../bin/wsiassembler seq

diff output/all.json expected/all.json
diff output/out_seq.json expected/out_seq.json
