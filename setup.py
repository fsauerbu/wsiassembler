"""
This script is used to install wsiassembler and all its dependencies. Run

    python setup.py install
or
    python3 setup.py install

to install the package.
"""

# Copyright (C) 2021 Frank Sauerburger

from setuptools import setup

def load_long_description(filename):
    """
    Loads the given file and returns its content.
    """
    with open(filename, encoding="utf-8") as readme_file:
        content = readme_file.read()
        return content

setup(name='wsiassembler',
      version='0.0.0',  # Also change in module
      packages=["wsiassembler", "wsiassembler.tests"],
      # Also add in requirements.txt
      install_requires=["numpy", "progressbar2", "pyyaml"],
      test_suite='wsiassembler.tests',
      scripts=['bin/wsiassembler'],
      description='',  # Short description
      long_description=load_long_description("README.rst"),
      url="https://gitlab.cern.ch/fsauerbu/wsiassembler",
      author="Frank Sauerburger",
      author_email="f.sauerburger@cern.ch",
      # keywords="physics value error uncertainty unit quantity",
      classifiers=[],  # https://pypi.org/classifiers/
      license="MIT")
