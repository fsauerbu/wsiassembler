# Copyright (C) 2021 Frank Sauerburger

"""
Module description
"""

from glob import glob
import math
import itertools
from progressbar import progressbar
from . import reader
from .errors import NoSuchHistogramError, NoSuchOperandError

__version__ = "0.0.0"  # Also change in setup.py

FILE_READER = {
    ".json": reader.JsonReader,
    ".root": reader.RootReader,
}

def op_divide_by(op_x, op_y):
    """divide_by transformation"""
    if isinstance(op_y, list):
        op_y = sum(op_y[1:], op_y[0])
    return op_x / op_y

def op_multiply_by(op_x, op_y):
    """multiply_by transformation"""
    if isinstance(op_y, list):
        op_y = sum(op_y[1:], op_y[0])
    return op_x * op_y

def op_divide_by_integral_of(op_x, op_y):
    """divide_by_integral_of transformation"""
    if isinstance(op_y, list):
        op_y = sum(_.integral() for _ in op_y)
    else:
        op_y = op_y.integral()
    return op_x / op_y

def op_multiply_by_integral_of(op_x, op_y):
    """multiply_by_integral_of transformation"""
    if isinstance(op_y, list):
        op_y = sum(_.integral() for _ in op_y)
    else:
        op_y = op_y.integral()
    return op_x * op_y


def fix_nan_with(default, fallback):
    """Replace NaN bins in default with fallback"""
    def has_any_nan(hist):
        return any(math.isnan(v) for v in hist.values)

    if has_any_nan(default):
        return default.patch_nan(fallback)

    return default

def zero_nan_if_zero_in(default, fallback):
    """Zero NaN bins in default with if fallback is zero"""
    def has_any_nan(hist):
        return any(math.isnan(v) for v in hist.values)

    if has_any_nan(default):
        return default.patch_nan_if_zero(fallback)

    return default

OPERATIONS = {
    "divide_by": op_divide_by,
    "multiply_by": op_multiply_by,
    "divide_by_integral_of": op_divide_by_integral_of,
    "multiply_by_integral_of": op_multiply_by_integral_of,
    "fix_nan_with": fix_nan_with,
    "zero_nan_if_zero_in": zero_nan_if_zero_in,
}


def hist_path(chcareg, sample, variation):
    """Convert a lookup parameters to a ROOT path"""
    tokens = [chcareg]
    if sample is not None:
        tokens.append(sample)
        if variation is not None:
            tokens.append(variation)

    return "/".join(tokens)

def lookup_params(path):
    """Extract the lookup parameters from a ROOT path"""
    tokens = path.split("/", 2)
    return tokens + [None] * (3 - len(tokens))

def get_reader(filename, *args, **kwds):
    """Return reader based on file extension"""
    for fext, cls in FILE_READER.items():
        if filename.lower().endswith(fext):
            return cls(filename, *args, **kwds)
    raise ValueError(f"No reader found for {filename}")

def parse_instructions(instructions):
    """Build the layer stack based on the parsed yaml config"""
    if "layers" not in instructions:
        raise ValueError("layers must be set in instructions")
    if "output_groups" not in instructions:
        raise ValueError("output_groups must be set in instructions")

    prev_layer = None
    for layer_data in reversed(instructions['layers']):
        this_layer = Layer(**layer_data)
        if prev_layer:
            this_layer.add_layer(prev_layer)
        prev_layer = this_layer

    output_groups = [
        Output(layer=prev_layer, **output_group)
        for output_group in instructions['output_groups']
    ]

    return prev_layer, output_groups


_cached_readers = {}
def get_cached_reader(filepath):
    """Global reader cache"""
    if filepath not in _cached_readers:
        _cached_readers[filepath] = get_reader(filepath)

    return _cached_readers[filepath]

class Layer:
    """
    Class representing a data layer hiding histograms in lower levels
    """

    # pylint: disable=too-many-arguments, too-many-instance-attributes
    def __init__(self, filepaths, slug=None, exclude=None, only=None,
                 transformation=None, reroute=None):
        """Create a new layer object"""
        self.filepaths = self._resolve_filepaths(filepaths)
        self.slug = slug
        self.filtr = Filter(exclude, only)
        self.sub_layer = None
        self.top_layer = self

        if transformation is None:
            transformation = []
        self.transformation = transformation

        if reroute is None:
            reroute = []
        self.reroute = reroute

        self.readers = {}  # Cache
        self.histograms = {}  # Cache
        self.merged_histograms = None  # Cache

    @staticmethod
    def _resolve_filepaths(paths):
        """Glob the given path or list of paths and return flat list"""
        if isinstance(paths, str):
            return glob(paths)

        return {expanded
                for path in paths
                for expanded in glob(path)}


    def lookup(self, chcareg, sample, variation, layer_slug=None):
        """Perform a histogram lookup and return (layer, histogram)"""
        if self._has_histogram(chcareg, sample, variation, layer_slug):
            return (self, *self._get_histogram(chcareg, sample, variation,
                                               layer_slug))

        if self.sub_layer:
            return self.sub_layer.lookup(chcareg, sample, variation,
                                         layer_slug)

        raise NoSuchHistogramError(f"[{layer_slug}] {chcareg} {sample} {variation} not found")

    def _reroute(self, chcareg, sample, variation):
        """Return a (ordered) list of rerouted parameters"""
        rerouted = []

        checks = {
            "variation_in": variation,
            "sample_in": sample,
            "chcareg_in": chcareg,
        }

        # Check if source matches
        for routing in self.reroute:
            if sorted(routing) != ["from", "to"]:
                raise ValueError("Invalid route")

            source = routing["from"]
            for check, value in checks.items():
                if check in source and value not in source[check]:
                    # source does not match
                    break
            else:
                # source match
                dest_chcareg = routing["to"].get("chcareg", chcareg)
                dest_variation = routing["to"].get("variation", variation)
                dest_samples = routing["to"].get("sample", sample)

                rerouted.append((dest_chcareg, dest_samples, dest_variation))
        return rerouted

    def _has_histogram(self, chcareg, sample, variation, layer_slug=None):
        """Return true if a histogram can be supplied by current layer"""
        if layer_slug is not None and layer_slug != self.slug:
            return False

        histograms = self._dir_layer()
        path = hist_path(chcareg, sample, variation)

        return path in histograms

    def _get_histogram(self, chcareg, sample, variation, layer_slug=None):
        """Return the filepath and the histogram"""
        # First check if rerouted exists
        original_param = (chcareg, sample, variation)
        rerouted_params = self._reroute(*original_param)
        rerouted_params.append(original_param)

        paths = [hist_path(*params) for params in rerouted_params]
        for path in paths:
            for filepath in self.filepaths:
                data_reader = self._get_cached_reader(filepath)
                try:
                    hist = data_reader.get_histogram(path)
                    hist = self._transform(hist, chcareg, sample, variation)
                    if path != paths[-1]:
                        # Be more verbose if rerouted
                        return f"{filepath}::{path}", hist
                    return filepath, hist

                except NoSuchHistogramError:
                    pass

        raise NoSuchHistogramError(f"Cannot find [{layer_slug}] {chcareg} {sample} {variation}")


    # pylint: disable=too-many-locals
    def _transform(self, histogram, chcareg, sample, variation):
        """Transform the given histogram"""
        for operation in self.transformation:
            op_name = operation["operation"]

            try:
                operand = self._get_operand(operation, histogram, chcareg,
                                            sample, variation)
            except NoSuchHistogramError as exception:
                raise NoSuchOperandError(
                        f"Cannot find operand for [{self.slug}] {chcareg} "
                        f"{sample} {variation}: {exception}"
                    ) from exception

            if op_name in OPERATIONS:
                histogram = OPERATIONS[op_name](histogram, operand)
            else:
                raise ValueError(f"Invalid operation {op_name} in {self}")

        return histogram

    @staticmethod
    def chan_cat_reg(chan_cat_reg):
        """Parse and return: channel, category, region"""
        parts = chan_cat_reg.split("__")
        if len(parts) != 3:
            return None, None, None

        chan, cat, reg = parts

        chan, chan_name = chan.split("_", 1)
        if chan != "chan":
            chan_name = None

        cat, cat_name = cat.split("_", 1)
        if cat != "cat":
            cat_name = None

        return chan_name, cat_name, reg


    def _get_operand(self, operation, histogram, chcareg, sample, variation):
        """Find and return the operand or raise an exception"""
        if "sum" in operation:
            summands = [self._get_operand(summand, histogram, chcareg,
                                          sample, variation)
                        for summand in operation["sum"]]

            if not summands:
                raise ValueError("List of summands cannot be empty. "
                                 f"[{self.slug}] {chcareg} "
                                 f"{sample} {variation}")
            return summands

        channel, cat_name, reg = self.chan_cat_reg(chcareg)
        op_channel = operation.get("channel", channel)
        op_cat = operation.get("category", cat_name)
        op_reg = operation.get("region", reg)

        if op_channel is None or op_cat is None or op_reg is None:
            op_chcareg = operation.get("chcareg", chcareg)
        else:
            op_chcareg = operation.get("chcareg",
                f"chan_{op_channel}__cat_{op_cat}__{op_reg}")
        op_sample = operation.get("sample", sample)
        op_variation = operation.get("variation", variation)
        op_slug = operation.get("slug")
        op_local = operation.get("layer-local", False)

        if op_local:
            if op_slug is not None:
                raise ValueError("Cannot set slug for layer-local "
                                 "operands")

            # Retrieve histogram without reroute, filter and transform
            path = hist_path(op_chcareg, op_sample, op_variation)
            for filepath in self.filepaths:
                data_reader = self._get_cached_reader(filepath)
                try:
                    operand = data_reader.get_histogram(path)
                    return operand
                except NoSuchHistogramError:
                    pass

            raise NoSuchHistogramError(
                f"Cannot layer-local [{op_slug}] {op_chcareg} "
                f"{op_sample} {op_variation}"
            )

        if (op_chcareg == chcareg
                and op_sample == sample
                and op_variation == variation
                and (op_slug is None or op_slug == self.slug)):
            # Self reference
            return histogram

        # Top layer lookup
        _, _, operand = self._top_lookup(
            op_chcareg, op_sample, op_variation,
            layer_slug=op_slug
        )
        return operand


    def _get_cached_reader(self, filepath):
        """Return a new or cached data reader for the given file path"""

        # Forward to global cache
        cached_reader = get_cached_reader(filepath)
        self.readers[filepath] = cached_reader
        return cached_reader

    def _reverse_route(self, chcareg, sample, variation):
        """Return set of paths which route to the given histogram"""
        source_paths = {hist_path(chcareg, sample, variation)}

        # If destination matches histogram in file
        checks = {
            "variation": variation,
            "sample": sample,
            "chcareg": chcareg,
        }

        for routing in self.reroute:
            if sorted(routing) != ["from", "to"]:
                raise ValueError("Invalid route")

            destination = routing["to"]
            for check, value in checks.items():
                if check in destination and value != destination[check]:
                    # destination does not match
                    break
            else:
                # All checks passed, the histogram is a destination
                # Compute product of source
                src_chcareg = routing["from"].get("chcareg_in", [chcareg])
                src_variation = routing["from"].get("variation_in",
                                                    [variation])
                src_samples = routing["from"].get("sample_in", [sample])
                source_paths.update(
                    hist_path(*params)
                    for params in itertools.product(src_chcareg,
                                                    src_samples,
                                                    src_variation)
                )

        return source_paths

    def _get_file_histograms(self, filepath):
        """Compute and cache filtered set of histograms"""
        if filepath not in self.histograms:
            data_reader = self._get_cached_reader(filepath)
            self.histograms[filepath] = set(
                name
                for data_name in data_reader.get_list_of_histograms()
                for name in self._reverse_route(*lookup_params(data_name))
                if self.filtr.survive(*lookup_params(name))
            )
        return self.histograms[filepath]

    def _dir_layer(self):
        """Return histograms present in current layer"""
        if self.merged_histograms is None:
            self.merged_histograms = set()
            for filepath in self.filepaths:
                file_histograms = self._get_file_histograms(filepath)

                # Check for overlap in this layer
                intersection = self.merged_histograms.intersection(
                    file_histograms
                )
                if intersection:
                    raise ValueError(f"{filepath} contains histograms "
                                     f"({', '.join(intersection)}) already "
                                      "see on in current layer.")
                self.merged_histograms.update(file_histograms)

        return self.merged_histograms

    def dir(self):
        """Return a complete list with all histograms from all layers"""
        histograms = self._dir_layer()

        if self.sub_layer:
            return histograms | self.sub_layer.dir()

        return histograms

    def add_layer(self, sub_layer):
        """Add a layer below the current layer"""
        self.sub_layer = sub_layer
        self.sub_layer.set_top(self.top_layer)

    def set_top(self, top_layer):
        """Set the top layer used for transform lookups"""
        self.top_layer = top_layer
        if self.sub_layer:
            self.sub_layer.set_top(top_layer)

    def _top_lookup(self, *args, **kwds):
        """Same as lookup but at top layer"""
        return self.top_layer.lookup(*args, **kwds)

    def __str__(self):
        if self.slug:
            return self.slug
        return super().__str__()


# pylint: disable=too-few-public-methods
class Filter:
    """
    Implementation of the filter logic based on 'exclude' and 'only' args.
    """
    VALID_CONDITIONS = ["variation_in", "sample_in", "chcareg_in",
                        "channel_in", "category_in", "region_in"]

    def __init__(self, exclude=None, only=None):
        """Create a new filter object"""
        if exclude is None:
            exclude = {}
        exclude_unprocessed = set(exclude).difference(self.VALID_CONDITIONS)
        if exclude_unprocessed:
            print("Ignoring unrecogniced 'exclude' conditions: "
                  + ", ".join(exclude_unprocessed))

        self.exclude = {k: set(v) for k, v in exclude.items()}

        if only is None:
            only = {}
        only_unprocessed = set(only).difference(self.VALID_CONDITIONS)
        if only_unprocessed:
            print("Ignoring unrecogniced 'only' conditions: "
                  + ", ".join(only_unprocessed))
        self.only = {k: set(v) for k, v in only.items()}

    def survive(self, chcareg, sample, variation):
        """Return true if the given lookup params pass the filter"""
        if not self.only and not self.exclude:
            return True

        channel, category, region = self.chan_cat_reg(chcareg)

        checks = {
            "variation_in": variation,
            "sample_in": sample,
            "chcareg_in": chcareg,
            "channel_in": channel,
            "category_in": category,
            "region_in": region,
        }

        for check, value in checks.items():
            # only
            if check in self.only and value not in self.only[check]:
                return False

            # exclude
            if check in self.exclude and value in self.exclude[check]:
                return False

        return True

    @staticmethod
    def chan_cat_reg(chan_cat_reg):
        """Parse and return: channel, category, region"""
        parts = chan_cat_reg.split("__")
        if len(parts) != 3:
            return None, None, None

        chan, cat, reg = parts

        chan, chan_name = chan.split("_", 1)
        if chan != "chan":
            chan_name = None

        cat, cat_name = cat.split("_", 1)
        if cat != "cat":
            cat_name = None

        return chan_name, cat_name, reg

class Output:
    """
    Wrapper of the outermost channel to filter the output
    """
    # pylint: disable=too-many-arguments
    def __init__(self, filepath, slug, layer, exclude=None, only=None):
        """Create an output wrapper"""
        self.layer = layer
        self.output_file = filepath
        self.histograms = None
        self.filtr = Filter(exclude, only)
        self.slug = slug

    def store(self, verbose=False):
        """Write all histograms of current view to new file"""
        with get_reader(self.output_file, mode="w") as data_writer:
            all_paths = self.dir()
            total = len(all_paths)
            wrapper = progressbar if not verbose else lambda x: x
            precomputed = list(enumerate(sorted(all_paths)))
            for i, path in wrapper(precomputed):
                layer, filename, hist = self.lookup(*lookup_params(path))
                if verbose:
                    print(f"[{i/total*100:5.1f}%] "
                          f"{str(layer):20} {path:50} ({filename})")
                data_writer.add_histogram(path, hist)


    def lookup(self, chcareg, sample, variation):
        """Delegate to top layer if it passes filter"""
        if hist_path(chcareg, sample, variation) in self.dir():
            return self.layer.lookup(chcareg, sample, variation)

        raise NoSuchHistogramError(chcareg, sample, variation)

    def dir(self):
        """Filtered dir() from top layer"""
        if self.histograms is None:
            unfiltered = self.layer.dir()
            self.histograms = set(
                name for name in unfiltered
                if self.filtr.survive(*lookup_params(name))
            )

        return self.histograms
