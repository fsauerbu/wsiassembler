
class NoSuchHistogramError(Exception):
    """Custom error class to indicate a failed lookup"""
    pass

class NoSuchOperandError(Exception):
    """Custom error class to indicate a failed operand lookup"""
    pass
