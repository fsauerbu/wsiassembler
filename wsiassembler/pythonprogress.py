
import sys
import math
import shutil

class DoingDone:
    """
    The DoingDone class provides a context manager to facilitate user
    communication about an action currently in progress and its resolution.
    """
    def __init__(self, message, success_message="done", fail_message="failed",
                 file=sys.stdout, fill_chars=" "):
        """
        Creates a context manager. The given message is printed directly. It
        tells the user which operation is executed during the following
        context block.

        The success_message is printed when the context block is exited
        regularly. The fail_message is printed when an occurring exception is
        not handled withing the context block. The default messages is "done"
        and "failed".

        By default, the messages are printed to stdout. An alternative file
        can be specified via the 'file' argument.

        If fill_chars evaluates to True, the fill_chars sequence is repeated
        such that the success of failure message is right aligned.
        """
        self.message = message
        self.success_message = success_message
        self.fail_message = fail_message
        self.file = file
        self.status_override = None
        self.fill_chars = fill_chars
        self.dots = "..."

    def fail(self):
        """
        Print the failure message regardless of any raised exception.
        """
        self.status_override = False

    def succeed(self):
        """
        Print the success message regardless of any raised exception.
        """
        self.status_override = True
    
    def __enter__(self):
        print(self.message, "...", sep="", end="", file=self.file)
        self.file.flush()
        return self


    def _fill(self, message):
        """
        Returns the repeated fill_char sequence to make the given
        success/failure message right aligned.

        If self.fill_chars evaluates to False, the terminal width is unknown
        or the terminal is too narrow, the method returns " ".
        """
        if not self.fill_chars:
            return " "

        terminal_length = shutil.get_terminal_size([None, None]).columns
        if terminal_length is None:
            return " "

        length = terminal_length - len(self.message) - len(message) - len(self.dots)
        if length <= 0:
            return " "

        repeat = math.ceil(length / len(self.fill_chars))
        return (self.fill_chars*repeat)[:length]

    def _finalize(self, message):
        """
        Prints the (right-aligned) message.
        """
        print(self._fill(message), message, sep="", file=self.file)

    def __exit__(self, type, value, traceback):
        success = (type is None)
        if self.status_override is not None:
            success = self.status_override

        if success:
            self._finalize(self.success_message)
        else:
            self._finalize(self.fail_message)
    

