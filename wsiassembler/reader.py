
"""
Abstract and concrete reader method to support multiple IO methods.

The io-interface classes are referred to as "readers" even though they are
capable of reading and writing data.
"""

from abc import ABC, abstractmethod
import math
import os
import types
import json
import numpy as np
import ROOT
from .errors import NoSuchHistogramError

#####################################################################
# Abstract base classes                                             #
#####################################################################

class HistogramReader(ABC):
    """Abstract histogram reader interface"""

    @abstractmethod
    def __init__(self, path, mode="r"):
        """Open input file in 'r' or 'w' mode"""

    @abstractmethod
    def get_list_of_histograms(self):
        """Return flat list of all histogram paths"""

    @abstractmethod
    def get_histogram(self, hist_path):
        """Return a histogram object"""

    @abstractmethod
    def add_histogram(self, hist_path, hist_obj):
        """Add a histogram object to the file"""

    @abstractmethod
    def close(self):
        """Relase the filesystem object and write changes"""

    def __enter__(self):
        """Context manager enter"""
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Context manager exit"""
        self.close()

    def __del__(self):
        """Close file at the every end"""
        self.close()

class Histogram(ABC):
    """Abstract histogram class with overloaded operators"""

    @abstractmethod
    def __truediv__(self, other):
        """Bin-by-bin histogram division or by a scalar"""

    @abstractmethod
    def __mul__(self, other):
        """Bin-by-bin histogram multiplication or by scalar"""
    @abstractmethod
    def __add__(self, other):
        """Bin-by-bin histogram addition"""

    @abstractmethod
    def integral(self):
        """Return the sum of all bins"""

    @abstractmethod
    def patch_nan(self, fallback):
        """Return a new histogram with NaN bins replaced by fallback"""

    @abstractmethod
    def patch_nan_if_zero(self, fallback):
        """Return a new histogram zeroed NaN if fallback is also zero"""

    @property
    def values(self):
        """Return the values as list"""
        pass

    @abstractmethod
    def show(self):
        """Show the content of the histogram"""

#####################################################################
# JSON-based implementation                                         #
#####################################################################

class JsonHistogram(Histogram):
    """See base class"""

    def __init__(self, values):
        """Stores values as bin content"""
        self.values = values

    def __truediv__(self, other):
        """Divide values item-by-item"""
        if isinstance(other, JsonHistogram):
            return JsonHistogram(self._values / other._values)
        return JsonHistogram(self._values / other)

    def __mul__(self, other):
        """Multiply values item-by-item"""
        if isinstance(other, JsonHistogram):
            return JsonHistogram(self._values * other._values)
        return JsonHistogram(self._values * other)

    def __add__(self, other):
        """Multiply values item-by-item"""
        return JsonHistogram(self._values + other._values)

    def integral(self):
        """Return sum of bins"""
        return self._values.sum()

    def show(self):
        """Print the bin content"""
        print(self.values)

    def patch_nan(self, fallback):
        """Return a new histogram with NaN bins replaced by fallback"""
        return JsonHistogram([f if math.isnan(v) else v
                              for v, f in zip(self.values, fallback.values)])

    def patch_nan_if_zero(self, fallback):
        """Return a new histogram zeroed NaN if fallback is also zero"""
        values = []
        for v, f in zip(self.values, fallback.values):
            if math.isnan(v):
                if f != 0:
                    raise ValueError("Would patch NaN with non-zero fallback "
                                     f"{f}")
                values.append(0)
            else:
                values.append(v)
        return JsonHistogram(values)

    @property
    def values(self):
        return self._values.tolist()

    @values.setter
    def values(self, new_values):
        self._values = np.asarray(new_values) 

class JsonReader(HistogramReader):
    """See base class"""

    def __init__(self, path, mode='r'):
        """Open JSON file for read or write"""
        if mode not in ['r', 'w']:
            raise ValueError("Reader mode must be 'r' or 'w'")

        self.file = open(path, mode)
        if mode == "r":
            self.data = json.load(self.file)
        else:
            self.data = {}

        self.mode = mode

    def close(self):
        """Close file"""
        if self.mode == "w" and not self.file.closed:
            json.dump(self.data, self.file)
        self.file.close()

    def get_list_of_histograms(self):
        """Return list of JSON keys"""
        return list(self.data.keys())

    def get_histogram(self, hist_path):
        """Retrieve values and return as histogram"""
        try:
            values = self.data[hist_path]
        except KeyError:
            raise NoSuchHistogramError()
        return JsonHistogram(values)

    def add_histogram(self, hist_path, hist_obj):
        """Add a histogram to the internal data dict"""
        if self.mode == "r":
            raise TypeError("Cannot write to read-only file")
        self.data[hist_path] = hist_obj.values

#####################################################################
# Root-based implementation                                         #
#####################################################################

REOPEN_THRESHOLD = 10_000

class RootReader(HistogramReader):
    """Abstract histogram reader interface"""

    def __init__(self, path, mode="r"):
        """Open input file in 'r' or 'w' mode"""
        ROOT.TH1.AddDirectory(False)
        self.mode = mode
        if mode == "r":
            r_mode = "READ"
        elif mode == "w":
            r_mode = "RECREATE"
        else:
            raise ValueError(f"Invalid mode {mode}")

        self.file = ROOT.TFile.Open(path, r_mode)
        self.histograms = set()
        self.path = path
        self.counter = 0

        if mode == "r":
            self._walk_dir_base(self.file, path)

    def cache_outdated(self, cache_file):
        source_time = os.path.getmtime(self.path)
        cache_time = os.path.getmtime(cache_file)

        is_outdated = source_time > cache_time
        return is_outdated

    def _walk_dir_base(self, file, path):
        """Load file index or walk directory"""
        dir_name = os.path.dirname(path)
        file_name = os.path.basename(path)
        cache_file = os.path.join(dir_name, f".{file_name}.json")
        try:
            if self.cache_outdated(cache_file):
                print(f"Cache for {self.path} is outdated. "
                       "Will recreate cache")
                raise OSError()

            with open(cache_file) as f:
                self.histograms = set(json.load(f))
        except OSError:
            self._walk_dir(self.file, prefix=[])

            with open(cache_file, "w") as f:
                json.dump(list(self.histograms), f)

    def _walk_dir(self, directory, prefix):
        """Walk the directory recursively and add histograms to list"""
        if not directory:
            return

        for key in directory.GetListOfKeys():
            obj = directory.Get(key.GetName())
            if not obj:
                continue
            name = prefix + [key.GetName()]
            if isinstance(obj, ROOT.TH1):
                self.histograms.add("/".join(name))
            elif isinstance(obj, ROOT.TDirectory):
                self._walk_dir(obj, prefix=name)
            obj.Delete()


    def get_list_of_histograms(self):
        """Return flat list of all histogram paths"""
        return self.histograms

    def get_histogram(self, hist_path):
        """Return a histogram object"""
        self.counter += 1
        if self.counter >= REOPEN_THRESHOLD:
            # Root histogram retrieval seems to degrade after some time.
            # Reopen to file to restore performance.
            self.counter = 0
            self.file.Close()
            self.file = ROOT.TFile.Open(self.path, "READ")

        th1 = self.file.Get(hist_path)
        if not th1:
            if hist_path in self.histograms:
                raise Exception("Root file closed prematurely! Probably a bug")
            raise NoSuchHistogramError(f"Histogram {hist_path} not found")

        return RootHistogram(th1)

    def add_histogram(self, hist_path, hist_obj):
        """Add a histogram object to the file"""
        if self.mode == "r":
            raise TypeError("Cannot write to read-only file")
        self.histograms.add(hist_path)

        self.counter += 1
        if self.counter >= REOPEN_THRESHOLD:
            # Root histogram retrieval seems to degrade after some time.
            # Reopen to file to restore performance.
            self.counter = 0
            self.file.Close()
            self.file = ROOT.TFile.Open(self.path, "UPDATE")


        *dirs, hist_name = hist_path.split("/")
        self.file.cd()

        if dirs:
            full_dir = "/".join(dirs)
            dir_obj = self.file.Get(full_dir)
            if not dir_obj:
                dir_obj = self.file.mkdir(full_dir)
                dir_obj = self.file.Get(full_dir)
            dir_obj.cd()

        try:
            th1 = hist_obj.th1.Clone()
            th1.SetName(hist_name)
            th1.SetDirectory(dir_obj)
            th1.Write()
            # th1.Delete()  # Don't delete since it's from Clone
        except AttributeError:
            print(f"Histogram for {hist_path} is null")

    def close(self):
        """Release the filesystem object and write changes"""
        if self.file:
            self.file.Close()

class RootHistogram(Histogram):
    """See base class"""
    def __init__(self, values, needs_cleanup=True):
        """Create a new TH1F wrapper"""
        self.needs_cleanup = needs_cleanup
        if isinstance(values, ROOT.TH1):
            self.th1 = values
        else:
            self.needs_cleanup = False
            self.th1 = ROOT.TH1F("", "", len(values), 0, len(values))
            for i, v in enumerate(values):
                self.th1.SetBinContent(i + 1, v)

    def __truediv__(self, other):
        """Divide values item-by-item"""
        copy = self.th1.Clone()

        if isinstance(other, RootHistogram):
            if not self.same_binning(other):
                raise ValueError("Binning not identical")
            copy.Divide(other.th1)
            for bin_i in range(copy.GetNbinsX() + 2):
                if abs(other.th1.GetBinContent(bin_i)) < 1.01e-6:
                    # Strong assumptions!
                    copy.SetBinContent(bin_i, 1)
            return RootHistogram(copy, needs_cleanup=False)

        if other == 0:
            copy.Scale(0)
        else:
            copy.Scale(1 / other)
        return RootHistogram(copy, needs_cleanup=False)


    def __mul__(self, other):
        """Bin-by-bin histogram multiplication or by scalar"""
        copy = self.th1.Clone()
        if isinstance(other, RootHistogram):
            if not self.same_binning(other):
                raise ValueError("Binning not identical")
            copy.Multiply(other.th1)
            return RootHistogram(copy, needs_cleanup=False)

        copy.Scale(other)
        return RootHistogram(copy, needs_cleanup=False)

    def __add__(self, other):
        """Bin-by-bin histogram summation"""
        copy = self.th1.Clone()
        if not self.same_binning(other):
            raise ValueError("Binning not identical")
        copy.Add(other.th1)
        return RootHistogram(copy, needs_cleanup=False)

    def same_binning(self, other):
        """Return true if both histos have the same binning"""
        bins = self.th1.GetNbinsX()
        if bins != other.th1.GetNbinsX():
            return False

        for i in range(bins + 1):
            if self.th1.GetBinLowEdge(i) != other.th1.GetBinLowEdge(i):
                return False

        return True

    def integral(self):
        """Return the sum of all bins"""
        return self.th1.Integral(0, self.th1.GetNbinsX() + 1)

    def patch_nan(self, fallback):
        """Return a new histogram with NaN bins replaced by fallback"""
        copy = self.th1.Clone()

        n_bins = copy.GetNbinsX()
        for bin_i in range(n_bins + 2):
            bin_value = copy.GetBinContent(bin_i)
            if math.isnan(bin_value):
                # Patch bin
                new_value = fallback.th1.GetBinContent(bin_i)
                copy.SetBinContent(bin_i, new_value)

                new_error = fallback.th1.GetBinError(bin_i)
                copy.SetBinError(bin_i, new_error)

        return RootHistogram(copy, needs_cleanup=False)

    def patch_nan_if_zero(self, fallback):
        """Return a new histogram zeroed NaN if fallback is also zero"""
        copy = self.th1.Clone()

        n_bins = copy.GetNbinsX()
        for bin_i in range(n_bins + 2):
            bin_value = copy.GetBinContent(bin_i)
            if math.isnan(bin_value):
                # Patch bin
                fallback_value = fallback.th1.GetBinContent(bin_i)
                if fallback_value != 0:
                    raise ValueError("Would patch NaN with non-zero fallback "
                                     f"{fallback_value}")

                copy.SetBinContent(bin_i, 0)
                copy.SetBinError(bin_i, 0)

        return RootHistogram(copy, needs_cleanup=False)

    @property
    def values(self):
        n_bins = self.th1.GetNbinsX()
        return [self.th1.GetBinContent(i + 1) for i in range(n_bins)]

    def show(self):
        """Draw the histogram"""
        self.th1.Draw()

    def __del__(self):
        """Delete internal TH1F"""
        if self.needs_cleanup:
            self.th1.Delete()
