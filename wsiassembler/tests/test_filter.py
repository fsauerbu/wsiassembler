
"""
Test the implementation of the filter classes
"""

import unittest
from wsiassembler import Filter

class FilterTestCase(unittest.TestCase):
    """Test the filter class"""

    def test_parse_valid(self):
        """Test the channel, category, region parsing"""
        chan, cat, reg = Filter.chan_cat_reg("chan_lh__cat_vbf_1__sr")
        self.assertEqual(chan, "lh")
        self.assertEqual(cat, "vbf_1")
        self.assertEqual(reg, "sr")

        chan, cat, reg = Filter.chan_cat_reg(
            "chan_hh__cat_boost_1J_2__cr_ztt_ee"
        )
        self.assertEqual(chan, "hh")
        self.assertEqual(cat, "boost_1J_2")
        self.assertEqual(reg, "cr_ztt_ee")

    def test_parse_invalid(self):
        """Test the channel, category, region parsing for invalid inputs"""
        chan, cat, reg = Filter.chan_cat_reg("chan_lh__catvbf_1__sr")
        self.assertEqual(chan, "lh")
        self.assertIsNone(cat)
        self.assertEqual(reg, "sr")

        chan, cat, reg = Filter.chan_cat_reg("chan_lh_catvbf_1__sr")
        self.assertIsNone(chan)
        self.assertIsNone(cat)
        self.assertIsNone(reg)

    def test_init_only(self):
        """Check that the 'only' conditions are stored"""
        filtr = Filter(only={"sample_in": ["Top"]})
        self.assertEqual(filtr.only, {"sample_in": {"Top"}})

    def test_init_exclude(self):
        """Check that the 'exclude' conditions are stored"""
        filtr = Filter(exclude={"sample_in": ["Top"]})
        self.assertEqual(filtr.exclude, {"sample_in": {"Top"}})

    def test_survive_only_channel(self):
        """Check that a only-condition on channel is evaluated"""
        filtr = Filter(only={"channel_in": ["lh"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_hh__cat_vbf_1__sr", "Top", "nominal")
        )

    def test_survive_only_cat(self):
        """Check that a only-condition on cat is evaluated"""
        filtr = Filter(only={"category_in": ["vbf_1"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_0__sr", "Top", "nominal")
        )

    def test_survive_only_region(self):
        """Check that a only-condition on region is evaluated"""
        filtr = Filter(only={"region_in": ["sr"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__cr_zrr", "Top", "nominal")
        )

    def test_survive_only_sample(self):
        """Check that a only-condition on region is evaluated"""
        filtr = Filter(only={"sample_in": ["Top"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Fake", "nominal")
        )

    def test_survive_only_variation(self):
        """Check that a only-condition on region is evaluated"""
        filtr = Filter(only={"variation_in": ["nominal"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "something_1up")
        )


    def test_survive_only_list(self):
        """Check that a only-condition lists are evaluated"""
        filtr = Filter(only={"sample_in": ["Top", "W"]})
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "W", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Fake", "something_1up")
        )


    def test_survive_only_multi(self):
        """Check that a multiple only-conditions are combined"""
        filtr = Filter(only={
            "sample_in": ["Top", "W"],
            "category_in": ["vbf_1"]
        })
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_0__sr", "W", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Fake", "nominal")
        )


    def test_survive_exclude_channel(self):
        """Check that a exclude-condition on channel is evaluated"""
        filtr = Filter(exclude={"channel_in": ["lh"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_hh__cat_vbf_1__sr", "Top", "nominal")
        )

    def test_survive_exclude_cat(self):
        """Check that a exclude-condition on cat is evaluated"""
        filtr = Filter(exclude={"category_in": ["vbf_1"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_0__sr", "Top", "nominal")
        )

    def test_survive_exclude_region(self):
        """Check that a exclude-condition on region is evaluated"""
        filtr = Filter(exclude={"region_in": ["sr"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__cr_zrr", "Top", "nominal")
        )

    def test_survive_exclude_sample(self):
        """Check that a exclude-condition on region is evaluated"""
        filtr = Filter(exclude={"sample_in": ["Top"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Fake", "nominal")
        )

    def test_survive_exclude_variation(self):
        """Check that a exclude-condition on region is evaluated"""
        filtr = Filter(exclude={"variation_in": ["nominal"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "something_1up")
        )


    def test_survive_exclude_list(self):
        """Check that a exclude-condition lists are evaluated"""
        filtr = Filter(exclude={"sample_in": ["Top", "W"]})
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "W", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Fake", "something_1up")
        )


    def test_survive_exclude_multi(self):
        """Check that a multiple exclude-conditions are combined"""
        filtr = Filter(exclude={
            "sample_in": ["Top", "W"],
            "category_in": ["vbf_1"]
        })
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        )
        self.assertFalse(
            filtr.survive("chan_lh__cat_vbf_0__sr", "W", "nominal")
        )
        self.assertTrue(
            filtr.survive("chan_lh__cat_vbf_0__sr", "Fake", "nominal")
        )


