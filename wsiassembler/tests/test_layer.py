
from pathlib import Path
import shutil
import tempfile
import os
import unittest

from wsiassembler import Layer, Filter, Output
from wsiassembler.errors import NoSuchHistogramError, NoSuchOperandError
from wsiassembler.reader import JsonReader, JsonHistogram

class LayerTestCase(unittest.TestCase):
    """
    Test the implementation of Layer class
    """
    def setUp(self):
        """Create toy directory used to glob"""
        self.tmpdir = tempfile.mkdtemp()

        self.touch("base.root")
        self.touch("add_1.root")
        self.touch("add_2.root")
        self.touch("add_3.root")
        self.touch("other.root")

    def touch(self, filename):
        """Touch a file an return its name"""
        filename = self.temp_filename(filename)
        Path(filename).touch()
        return filename

    def temp_filename(self, filename):
        """Build full path of temporary file"""
        return os.path.join(self.tmpdir, filename)

    def tearDown(self):
        """Remote temporary directory"""
        shutil.rmtree(self.tmpdir)

    def hist(self, filename, hist_list):
        """Add histograms to file"""
        with JsonReader(filename, "w") as reader:
            for hist_path, hist_values in hist_list:
                hist_obj = JsonHistogram(hist_values)
                reader.add_histogram(hist_path, hist_obj)

    def test_init_glob_single(self):
        """Check that init globs single path arg"""
        this_file = self.touch("this.root")

        layer = Layer(this_file)
        self.assertEqual(layer.filepaths, [this_file])

    def test_init_glob_multiple(self):
        """Check that init globs list path arg"""
        this_file = self.touch("this.root")
        add_files = self.temp_filename("add_*.root")

        layer = Layer([this_file, add_files])
        self.assertEqual(sorted(layer.filepaths),
                        [self.temp_filename("add_1.root"),
                         self.temp_filename("add_2.root"),
                         self.temp_filename("add_3.root"),
                         this_file])
                                           

    def test_init_slug(self):
        """Check that the slug is stored internally"""
        this_file = self.touch("this.root")
        
        layer = Layer([this_file], "on-off-file")
        self.assertEqual(layer.slug, "on-off-file")

    def test_init_filter_only(self):
        """Check that a filter object is built"""
        this_file = self.touch("this.root")
        
        layer = Layer([this_file], only={"variation_in": ["nominal"]})
        self.assertIsInstance(layer.filtr, Filter)

    def test_init_filter_exclude(self):
        """Check that a filter object is built"""
        this_file = self.touch("this.root")
        
        layer = Layer([this_file], exclude={"variation_in": ["nominal"]})
        self.assertIsInstance(layer.filtr, Filter)

    def test_init_sub_layer(self):
        """Check that init() initialized sub_layer to None"""
        this_file = self.touch("this.root")
        layer = Layer(this_file)

        self.assertIsNone(layer.sub_layer)
        
    def test_init_top_layer(self):
        """Check that init() initialized top_layer to self"""
        this_file = self.touch("this.root")
        layer = Layer(this_file)

        self.assertEqual(layer.top_layer, layer)
        
    def test_add_layer(self):
        """Test add_layer sets sub_layer property"""
        base_file = self.touch("base.root")
        base_layer = Layer(base_file)

        top_file = self.touch("top.root")
        top_layer = Layer(top_file)
        top_layer.add_layer(base_layer)

        self.assertEqual(top_layer.sub_layer, base_layer)
        self.assertIsNone(base_layer.sub_layer)

    def test_add_layer_set_top_layer(self):
        """Test add_layer sets top_layer property"""
        base_file = self.touch("base.root")
        base_layer = Layer(base_file)

        mid_file = self.touch("mid.root")
        mid_layer = Layer(mid_file)
        mid_layer.add_layer(base_layer)

        top_file = self.touch("top.root")
        top_layer = Layer(top_file)
        top_layer.add_layer(mid_layer)

        self.assertEqual(top_layer.top_layer,  top_layer)
        self.assertEqual(mid_layer.top_layer,  top_layer)
        self.assertEqual(base_layer.top_layer, top_layer)

        self.assertEqual(top_layer.sub_layer,  mid_layer)
        self.assertEqual(mid_layer.sub_layer,  base_layer)
        self.assertIsNone(base_layer.sub_layer)

    def test_add_layer_set_top_layer_tail_append(self):
        """Test tail-adding layers sets top_layer property"""
        top_file = self.touch("top.root")
        top_layer = Layer(top_file)

        mid_file = self.touch("mid.root")
        mid_layer = Layer(mid_file)
        top_layer.add_layer(mid_layer)

        base_file = self.touch("base.root")
        base_layer = Layer(base_file)
        mid_layer.add_layer(base_layer)

        self.assertEqual(top_layer.top_layer,  top_layer)
        self.assertEqual(mid_layer.top_layer,  top_layer)
        self.assertEqual(base_layer.top_layer, top_layer)

        self.assertEqual(top_layer.sub_layer,  mid_layer)
        self.assertEqual(mid_layer.sub_layer,  base_layer)
        self.assertIsNone(base_layer.sub_layer)

    def test_dir_single(self):
        """Check that a single layer lists all histograms"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/struct/histxxx", [1, 2, 3, 4]),
            ("some/dir/struct/histxxy", [1, 2, 3, 5]),
            ("some/other/struct/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("different/dir/struct/histxxx", [1, 3, 3, 4]),
            ("different/dir/struct/histxxy", [1, 4, 3, 4]),
        ])

        layer = Layer([alpha, beta])
        self.assertEqual(sorted(layer.dir()), [
            "different/dir/struct/histxxx",
            "different/dir/struct/histxxy",
            "some/dir/struct/histxxx",
            "some/dir/struct/histxxy",
            "some/other/struct/nominal",
        ])
                         

    def test_dir_single_conflict(self):
        """Check that an error is raised if name occures twice in layer"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/struct/histxxx", [1, 2, 3, 4]),
            ("some/dir/struct/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/struct/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/struct/nominal", [1, 3, 3, 4]),
            ("different/dir/struct/histxxy", [1, 4, 3, 4]),
        ])

        layer = Layer([alpha, beta])
        self.assertRaises(ValueError, layer.dir)

    def test_dir_multiple(self):
        """Check that the histogram from the top layer has precedence"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/nominal", [1, 3, 3, 4]),
            ("different/dir/histxxy", [1, 4, 3, 4]),
        ])

        sub_layer = Layer(beta)

        top_layer = Layer(alpha)
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "duplicate", "nominal") 
        self.assertEqual(layer, top_layer)
        self.assertEqual(filename, alpha)
        self.assertEqual(hist.values, [7, 6, 5, 1])

        layer, filename, hist = top_layer.lookup("different", "dir", "histxxy") 
        self.assertEqual(layer, sub_layer)
        self.assertEqual(filename, beta)
        self.assertEqual(hist.values, [1, 4, 3, 4])

    def test_dir_multiple_only(self):
        """Check that the histogram from the top layer has precedence"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/nominal", [1, 3, 3, 4]),
            ("different/dir/histxxy", [1, 4, 3, 4]),
        ])

        sub_layer = Layer(beta)

        top_layer = Layer(alpha, only={'sample_in': ['dir']})
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "duplicate", "nominal") 
        self.assertEqual(layer, sub_layer)
        self.assertEqual(filename, beta)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx") 
        self.assertEqual(layer, top_layer)
        self.assertEqual(filename, alpha)

    def test_dir_multiple_exclude(self):
        """Check that the histogram from the top layer has precedence"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/nominal", [1, 3, 3, 4]),
            ("different/dir/histxxy", [1, 4, 3, 4]),
        ])

        sub_layer = Layer(beta)

        top_layer = Layer(alpha, exclude={'sample_in': ['duplicate']})
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "duplicate", "nominal")
        self.assertEqual(layer, sub_layer)
        self.assertEqual(filename, beta)
        self.assertEqual(hist.values, [1, 3, 3, 4])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx") 
        self.assertEqual(layer, top_layer)
        self.assertEqual(filename, alpha)
        self.assertEqual(hist.values, [1, 2, 3, 4])

    def test_get_histogram(self):
        """Check that return value of _get_histogram with one layer"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        layer = Layer(alpha)

        filename, hist = layer._get_histogram("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 2, 3, 5])
        self.assertEqual(filename, alpha)

    def test_get_histogram_fail(self):
        """Check _get_histogram raises for non-existent histogram"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        layer = Layer(alpha)

        self.assertRaises(NoSuchHistogramError,
                         layer._get_histogram, "some", "dir", "no-such-thing")

    def test_store_single(self):
        """Check that all histograms of a single layer are stored"""
        alpha = self.temp_filename("alpha.json")

        data = [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ]
        self.hist(alpha, data)
        layer = Layer(alpha)

        copy = self.temp_filename("copy.json")
        output = Output(copy, slug="copy", layer=layer)
        output.store()

        with JsonReader(copy) as reader:
            self.assertEqual(len(reader.get_list_of_histograms()),
                             len(data))
            for path, values in data:
                hist = reader.get_histogram(path)
                self.assertEqual(hist.values, values)

    def test_store_multi(self):
        """Check that all histograms of a multiple layers are stored"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/nominal", [1, 3, 3, 4]),
            ("different/dir/histxxy", [1, 4, 3, 4]),
        ])

        sub_layer = Layer(beta)

        top_layer = Layer(alpha)
        top_layer.add_layer(sub_layer)

        output = self.temp_filename("output.json")
        Output(output, slug="output", layer=top_layer).store()

        with JsonReader(output) as reader:
            self.assertEqual(len(reader.get_list_of_histograms()), 4)

            hist = reader.get_histogram("some/dir/histxxx")
            self.assertEqual(hist.values, [1, 2, 3, 4])

            hist = reader.get_histogram("some/dir/histxxy")
            self.assertEqual(hist.values, [1, 2, 3, 5])

            hist = reader.get_histogram("some/duplicate/nominal")
            self.assertEqual(hist.values, [7, 6, 5, 1])

            hist = reader.get_histogram("different/dir/histxxy")
            self.assertEqual(hist.values, [1, 4, 3, 4])

    def test_store_multi_filter(self):
        """Check that all histograms of a multiple layers are stored"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
            ("some/duplicate/nominal", [7, 6, 5, 1]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/duplicate/nominal", [1, 3, 3, 4]),
            ("different/dir/histxxy", [1, 4, 3, 4]),
        ])

        sub_layer = Layer(beta)

        top_layer = Layer(alpha, only={"sample_in": ['dir']})
        top_layer.add_layer(sub_layer)

        output = self.temp_filename("output.json")
        Output(output, slug="output", layer=top_layer).store()

        with JsonReader(output) as reader:
            self.assertEqual(len(reader.get_list_of_histograms()), 4)

            hist = reader.get_histogram("some/dir/histxxx")
            self.assertEqual(hist.values, [1, 2, 3, 4])

            hist = reader.get_histogram("some/dir/histxxy")
            self.assertEqual(hist.values, [1, 2, 3, 5])

            hist = reader.get_histogram("some/duplicate/nominal")
            self.assertEqual(hist.values, [1, 3, 3, 4])

            hist = reader.get_histogram("different/dir/histxxy")
            self.assertEqual(hist.values, [1, 4, 3, 4])

    def test_divide_by(self):
        """Check that divide_by transformations divide the histogram"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/nominal", [2, 2, 4, 4]),
        ])

        sub_layer = Layer(beta)
        top_layer = Layer(alpha, transformation=[
            {"operation": "divide_by", "variation": "nominal"}
        ])
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [0.5, 1, 0.75, 1])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [0.5, 1, 0.75, 1.25])

        layer, filename, hist = top_layer.lookup("some", "dir", "nominal")
        self.assertEqual(hist.values, [2, 2, 4, 4])

    def test_multiply_by(self):
        """Check that multiply_by transformations multiplies the histogram"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/nominal", [2, 2, 4, 4]),
        ])

        sub_layer = Layer(beta)
        top_layer = Layer(alpha, transformation=[
            {"operation": "multiply_by", "variation": "nominal"}
        ])
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [2, 4, 12, 16])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [2, 4, 12, 20])

        layer, filename, hist = top_layer.lookup("some", "dir", "nominal")
        self.assertEqual(hist.values, [2, 2, 4, 4])

    def test_divide_by_integral_of(self):
        """Check that divide_by_integral_of transformations divide the histogram"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/nominal", [1, 1, 0, 2]),
        ])

        sub_layer = Layer(beta)
        top_layer = Layer(alpha, transformation=[
            {"operation": "divide_by_integral_of", "variation": "nominal"}
        ])
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [0.25, 0.5, 0.75, 1])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [0.25, 0.5, 0.75, 1.25])

        layer, filename, hist = top_layer.lookup("some", "dir", "nominal")
        self.assertEqual(hist.values, [1, 1, 0, 2])

    def test_multiply_by_integral_of(self):
        """Check that multiply_by_integral_of transformations scales the histogram"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/nominal", [-1, 1, 0, 0.25]),
        ])

        sub_layer = Layer(beta)
        top_layer = Layer(alpha, transformation=[
            {"operation": "multiply_by_integral_of", "variation": "nominal"}
        ])
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [0.25, 0.5, 0.75, 1])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [0.25, 0.5, 0.75, 1.25])

        layer, filename, hist = top_layer.lookup("some", "dir", "nominal")
        self.assertEqual(hist.values, [-1, 1, 0, 0.25])

    def test_reroute_dir(self):
        """Check that rerouting adds histograms to dir()"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        layer = Layer(alpha, reroute=[{
            "from": {"variation_in": ["histxxx_new"]},
            "to":   {"variation": "histxxx"}
        }])

        paths = layer.dir()
        self.assertEqual(sorted(paths), [
            "some/dir/histxxx",
            "some/dir/histxxx_new",
            "some/dir/histxxy",
        ])

    def test_reroute_get(self):
        """Check that rerouted histograms are retrievable"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        layer = Layer(alpha, reroute=[{
            "from": {"variation_in": ["histxxx_new"]},
            "to":   {"variation": "histxxx"}
        }])

        layer, filename, hist = layer.lookup("some", "dir", "histxxx_new")
        self.assertEqual(hist.values, [1, 2, 3, 4])

        layer, filename, hist = layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [1, 2, 3, 4])

        layer, filename, hist = layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 2, 3, 5])

    def test_transform_self(self):
        """Check that a histogram can use itself in a transformation"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        top_layer = Layer(alpha, transformation=[{
            "operation": "divide_by",
            "variation": "histxxx",
            # Referencing a histogram that's never actually seen
            "slug": "beta"
        }], slug="alpha")

        sub_layer = Layer(alpha, slug="beta")
        top_layer.add_layer(sub_layer)

        # Normalize to other
        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 1, 1, 1.25])

        # Normalize to self
        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [1, 1, 1, 1])

    def test_transform_local_first(self):
        """Check that a transformation gives precedence to the local hist"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [-1, 1, 0, 0.25]),
        ])
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        top_layer = Layer(alpha)
        sub_layer = Layer(beta, slug="beta", transformation=[{
            "operation": "divide_by",
            "variation": "histxxx",
            "layer-local": True,
        }])
        top_layer.add_layer(sub_layer)

        # Prioritize local
        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 1, 1, 1.25])

    def test_transform_top_fallback(self):
        """Check that a transformation falls back to top lookup"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/top", [-1, 1, 1, 0.25]),
        ])
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        top_layer = Layer(alpha)
        sub_layer = Layer(beta, transformation=[{
            "operation": "divide_by",
            "variation": "top"
        }])
        top_layer.add_layer(sub_layer)

        # Prioritize fallback top
        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [-1, 2, 3, 16])

    def test_transform_normalize_self(self):
        """Check that a histograms can be normlized"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 4, 3, 2]),
        ])

        top_layer = Layer(alpha, transformation=[{
            "operation": "divide_by_integral_of",
        }], slug="alpha")

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [0.1, 0.2, 0.3, 0.4])

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [0.1, 0.4, 0.3, 0.2])


    def test_transform_layer_local(self):
        """Check that a transformation can reference layer-local histograms"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [-1, 1, 0, 0.25]),
        ])
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        top_layer = Layer(alpha)
        sub_layer = Layer(beta, slug="beta", transformation=[{
            "operation": "divide_by",
            "variation": "histxxx",
            "layer-local": True,
        }])
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 1, 1, 1.25])

        # Top level access
        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [-1, 1, 0, 0.25])

    def test_transform_layer_local_filter(self):
        """Check that a transformation can reference layer-local histograms"""
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        layer = Layer(beta, slug="beta",
            only={"variation_in": ["histxxy"]},
            transformation=[{
                "operation": "divide_by",
                "variation": "histxxx",
                "layer-local": True,
            }])

        # Skip Filter
        layer, filename, hist = layer.lookup("some", "dir", "histxxy")
        self.assertEqual(hist.values, [1, 1, 1, 1.25])

    def test_transform_operand_not_found(self):
        """Check that NoSuchHistogramError is raised"""
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/histxxx", [1, 2, 3, 4]),
            ("some/dir/histxxy", [1, 2, 3, 5]),
        ])

        layer = Layer(beta, slug="beta",
            only={"variation_in": ["histxxy"]},
            transformation=[{
                "operation": "divide_by",
                "variation": "no-such-thing",
            }])

        # Skip Filter
        self.assertRaises(NoSuchOperandError, layer.lookup,
                          "some", "dir", "histxxy")

    def test_transform_sum(self):
        """Check that a transformation can have a sum operand"""
        alpha = self.temp_filename("alpha.json")
        self.hist(alpha, [
            ("some/dir/histxxx", [10, 20, 30, 40]),
        ])
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/a", [1, 2, 3, 4]),
            ("some/dir/b", [3, 2, 7, 16]),
        ])

        top_layer = Layer(alpha, transformation=[{
            "operation": "divide_by",
            "sum": [
                {"variation": "a"},
                {"variation": "b"},
            ]
        }])
        sub_layer = Layer(beta, slug="beta")
        top_layer.add_layer(sub_layer)

        layer, filename, hist = top_layer.lookup("some", "dir", "histxxx")
        self.assertEqual(hist.values, [2.5, 5, 3, 2])

    def test_transform_fix_nan(self):
        """Check that nan bins are replaced with the operand"""
        beta = self.temp_filename("beta.json")
        self.hist(beta, [
            ("some/dir/a", [1, float('nan'), 3, 4]),
            ("some/dir/b", [3, 2, 7, 16]),
        ])

        top_layer = Layer(beta, transformation=[{
            "operation": "fix_nan_with",
            "variation": "b"
        }])

        layer, filename, hist = top_layer.lookup("some", "dir", "a")
        self.assertEqual(hist.values, [1, 2, 3, 4])
