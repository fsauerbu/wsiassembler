
# Copyright (C) 2021 Frank Sauerburger

import unittest
from wsiassembler import __version__

class DemoTestCase(unittest.TestCase):
    """
    Test something.
    """

    def test_version(self):
        """
        Check that the version string in the module is 0.0.0.
        """
        self.assertEqual(__version__, "0.0.0")