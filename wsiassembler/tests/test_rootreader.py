
"""
Test the implementation of the JSON IO classes
"""

import os
import tempfile
import unittest
import ROOT

from wsiassembler.reader import RootReader
from wsiassembler.reader import RootHistogram as Histogram
from wsiassembler.errors import NoSuchHistogramError

class RootHistogramTestCase(unittest.TestCase):
    """Test the Root histogram class"""

    def test_init(self):
        """Check that the bins contents are stored"""
        hist = Histogram([1, 2, 3, 1])
        self.assertEqual(hist.values,
                         [1, 2, 3, 1])

    def test_divide(self):
        """Check that histograms can be divided"""
        num = Histogram([1, 2, 3, 1])
        den = Histogram([1, 0.5, 2, 4])

        ratio = num / den
        self.assertEqual(ratio.values,
                         [1, 4, 1.5, 0.25])

    def test_divide_binning(self):
        """Check that histograms can be divided"""
        num = Histogram([1, 2, 1])
        den = Histogram([1, 0.5, 2, 4])

        self.assertRaises(ValueError, lambda: num / den)

    def test_divide_binning_pos(self):
        """Check that histograms can be multiplied"""
        num = Histogram([1, 2, 1])
        den = Histogram([1, 0.5, 2, 4, 5, 6])
        den.th1.Rebin(2)

        self.assertRaises(ValueError, lambda: num / den)

    def test_divide_scalar(self):
        """Check that histograms can be divided by scalar"""
        num = Histogram([1, 2, 3, 1])
        den = 4

        ratio = num / den
        self.assertEqual(ratio.values, [0.25, 0.5, 0.75, 0.25])

    def test_mul_scalar(self):
        """Check that histograms can be multiplied by scalar"""
        num = Histogram([1, 2, 3, 1])
        factor = 4

        product = num * factor
        self.assertEqual(product.values, [4, 8, 12, 4])

    def test_mul(self):
        """Check that histograms can be multiplied"""
        a = Histogram([1, 2, 3, 1])
        b = Histogram([1, 0.5, 2, 4])

        product = a * b
        self.assertEqual(product.values,
                         [1, 1, 6, 4])

    def test_mul_binning(self):
        """Check that histograms can be multiplied"""
        num = Histogram([1, 2, 1])
        den = Histogram([1, 0.5, 2, 4])

        self.assertRaises(ValueError, lambda: num * den)

    def test_intergral(self):
        """Check that histograms can be integrated"""
        a = Histogram([1, 2, 3, 1])
        self.assertEqual(a.integral(), 7)

    def test_add(self):
        """Check that histograms can be summed"""
        a = Histogram([1, 2, 3, 1])
        b = Histogram([1, 0.5, 2, 4])

        product = a + b
        self.assertEqual(product.values,
                         [2, 2.5, 5, 5])

    def test_intergral_outside(self):
        """Check that integral includes under and overflow"""
        th1 = ROOT.TH1F("", "", 20, 0, 1)
        th1.Fill(-1)
        th1.Fill(2)
        a = Histogram(th1)
        self.assertEqual(a.integral(), 2)


class RootReaderTestCase(unittest.TestCase):
    """Test the Root histogram reader class"""

    def setUp(self):
        """Create toy directory used to glob"""
        _, self.tmpfile = tempfile.mkstemp()

    def tearDown(self):
        """Remote temporary directory"""
        os.remove(self.tmpfile)

    def test_list(self):
        """Check that written histograms are listed"""
        reader = RootReader(self.tmpfile, "w")
        reader.add_histogram("some/hist/here", Histogram([1, 2, 3, 4]))
        reader.add_histogram("some/hist/there", Histogram([1, 2, 3, 0]))
        reader.add_histogram("some_hist/nowhere", Histogram([1, 4, 9, 0]))
        reader.close()

        reader = RootReader(self.tmpfile)
        self.assertEqual(sorted(reader.get_list_of_histograms()),
                         ["some/hist/here",
                          "some/hist/there",
                          "some_hist/nowhere"])
        reader.close()

    def test_hist(self):
        """Check that written histogram can be retrieved"""
        reader = RootReader(self.tmpfile, "w")
        reader.add_histogram("some/hist/here", Histogram([1, 2, 3, 4]))
        reader.add_histogram("some/hist/there", Histogram([1, 2, 3, 0]))
        reader.add_histogram("some_hist/nowhere", Histogram([1, 4, 9, 0]))
        reader.close()

        reader = RootReader(self.tmpfile)
        hist_there = reader.get_histogram("some/hist/there")
        self.assertEqual(hist_there.values,
                         [1, 2, 3, 0])
        reader.close()

    def test_get_non_existent(self):
        """Check that a KeyError is raised if a histogram does not exist"""
        reader = RootReader(self.tmpfile, "w")
        reader.add_histogram("some/hist/here", Histogram([1, 2, 3, 4]))
        reader.add_histogram("some/hist/there", Histogram([1, 2, 3, 0]))
        reader.add_histogram("some_hist/nowhere", Histogram([1, 4, 9, 0]))
        reader.close()

        reader = RootReader(self.tmpfile)
        self.assertRaises(NoSuchHistogramError,
                          reader.get_histogram, "No such thing")
        reader.close()

    def test_rmode_add(self):
        """Check that a file opened in 'r' mode does not permit add"""
        reader = RootReader(self.tmpfile, "w")
        reader.close()

        reader = RootReader(self.tmpfile)
        self.assertRaises(TypeError,
                          reader.add_histogram, "path", Histogram([1,2]))
        reader.close()


    def test_wmode_read(self):
        """Check that a file opened in 'w' mode has no histograms"""
        reader = RootReader(self.tmpfile, 'w')
        self.assertEqual(reader.get_list_of_histograms(), set())
        reader.close()

    def test_context_manager(self):
        """Check that the context manger creates a context"""
        with RootReader(self.tmpfile, "w") as reader:
            reader.add_histogram("some/hist/here", Histogram([1, 2, 3, 4]))
            reader.add_histogram("some/hist/there", Histogram([1, 2, 3, 0]))
            reader.add_histogram("some_hist/nowhere", Histogram([1, 4, 9, 0]))

        with RootReader(self.tmpfile) as reader:
            self.assertEqual(sorted(reader.get_list_of_histograms()),
                             ["some/hist/here",
                              "some/hist/there",
                              "some_hist/nowhere"])

    def test_patch_nan(self):
        """Check that Nan values are patched with the fallback histogram"""
        first = Histogram([1, 2, float('nan'), 0])
        second = Histogram([10, 11, 12, 13])

        fixed = first.patch_nan(second)
        self.assertEqual(fixed.values, [1, 2, 12, 0])

    def test_zero_nan_if_zero(self):
        """Check that Nan values are zeroed if the fallback is zero"""
        first = Histogram([1, 2, float('nan'), 0])
        second = Histogram([10, 11, 0, 13])

        fixed = first.patch_nan_if_zero(second)
        self.assertEqual(fixed.values, [1, 2, 0, 0])

    def test_zero_nan_if_nonzero(self):
        """Check that an exception is raised if the fallback is not zero"""
        first = Histogram([1, 2, float('nan'), 0])
        second = Histogram([10, 11, 1, 13])

        self.assertRaises(ValueError, first.patch_nan_if_zero, second)
