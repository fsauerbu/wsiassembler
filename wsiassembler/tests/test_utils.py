
import unittest
import wsiassembler as asm

class NameParamsConverterTestCase(unittest.TestCase):
    """
    Test the implementation of the helper functions converting histogram paths
    to lookup params and vice versa
    """

    def test_hist_path(self):
        """Test hist_path() returns the histogram path for given params"""
        path = asm.hist_path("chan_lh__cat_vbf_1__sr", "Top", "nominal")
        self.assertEqual(path, "chan_lh__cat_vbf_1__sr/Top/nominal")
    
    def test_hist_path_truncated(self):
        """Test that hist_path() truncates trailing empty params"""
        path = asm.hist_path("chan_lh__cat_vbf_1__sr", "Top", None)
        self.assertEqual(path, "chan_lh__cat_vbf_1__sr/Top")

        path = asm.hist_path("chan_lh__cat_vbf_1__sr", None, None)
        self.assertEqual(path, "chan_lh__cat_vbf_1__sr")

    def test_lookup_params(self):
        """Test lookup_params() returns params for given path"""
        params = asm.lookup_params("chan_lh__cat_vbf_1__sr/Top/nominal")
        self.assertEqual(params, ["chan_lh__cat_vbf_1__sr", "Top", "nominal"])

    def test_lookup_params_truncated(self):
        """Test lookup_params() returns truncated params for truncated path"""
        params = asm.lookup_params("chan_lh__cat_vbf_1__sr/Top")
        self.assertEqual(params, ["chan_lh__cat_vbf_1__sr", "Top", None])
    
        params = asm.lookup_params("chan_lh__cat_vbf_1__sr")
        self.assertEqual(params, ["chan_lh__cat_vbf_1__sr", None, None])
    
